var gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    del = require('del'),
    htmlmin = require('gulp-htmlmin'),
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify');

/*
    ----------------------------------------------------------------------------------------------------
    - Development Tasks
    ----------------------------------------------------------------------------------------------------
*/

/*
    Start the development server.
*/
gulp.task('dev:connect', function () {

    connect.server({ root: ['./wwwroot/'], port: 8080, livereload: true });

});

/*
    Reference the Stylesheet and JavaScript files by injecting the paths into index.html.
*/
gulp.task('dev:inject-app', function () {

    var target = gulp.src('./wwwroot/*.html');
    var sources = gulp.src(['./wwwroot/components/*core*/*', './wwwroot/components/**/*.*'], { read: false });

    return target.pipe(inject(sources, { name: 'app', relative: true, empty: true }))
        .pipe(gulp.dest('./wwwroot'))
        .pipe(connect.reload());

});

/*
    Watch for any changes to the 'components' directory.
*/
gulp.task('dev:watch', function () {

    return gulp.watch(['./wwwroot/components/**/*.*'], gulp.series('dev:inject-app'));

});

/*
  Run all 'dev' tasks.
*/
gulp.task('dev', gulp.series('dev:inject-app', gulp.parallel('dev:connect', 'dev:watch')));

/*
    ----------------------------------------------------------------------------------------------------
    - Distribution Tasks
    ----------------------------------------------------------------------------------------------------
*/

/*
    Remove the 'dist' directory.
*/
gulp.task('dist:clean', function(){

    return del('../dist/**', { force:true });

});

/* 
    Copy all files excluding Stylesheet JavaScript libraries.
*/
gulp.task('dist:copy', function () {

    return gulp.src(['./wwwroot/**/*', '!./wwwroot/**/*.css', '!./wwwroot/**/*.js'], { nodir: true })
        .pipe(gulp.dest('../dist/wwwroot'));

});

/*
    Bundle and minify the application Stylesheet files into a single file.
*/
gulp.task('dist:css', function () {

    return gulp.src(['./wwwroot/**/*.css'])
        .pipe(concat('app.bundle.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('../dist/wwwroot/assets/css'));

});

/*
    Bundle and minify the application JavaScript files into a single file.
*/
gulp.task('dist:js', function () {

    return gulp.src(['./wwwroot/**/*.js'])
        .pipe(concat('app.bundle.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../dist/wwwroot/assets/js'));

});

/*
    Reference the Stylesheet and JavaScript bundles by injecting the paths into index.html.
*/
gulp.task('dist:inject-app', function () {

    var target = gulp.src('../dist/wwwroot/*.html');
    var sources = gulp.src(['../dist/wwwroot/**/*'], { read: false });

    return target.pipe(inject(sources, { name: 'app', relative: true, empty: true }))
        .pipe(gulp.dest('../dist/wwwroot'));

});

/*
    Clean up and compress all HTML files.
*/
gulp.task('dist:html', function() {

    return gulp.src('../dist/**/*.html')
      .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
      .pipe(gulp.dest('../dist'));

  });

/*
  Run all 'dist' tasks.
*/
gulp.task('dist', gulp.series('dist:clean', 'dist:copy', 'dist:css', 'dist:js', gulp.series('dist:inject-app', 'dist:html')));