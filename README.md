# HTML Boilerplate

This is just a basic HTML bolierplate that I use for hacking around with. 

## Applications

| Name                            | Version       |
| :------------------------------ |:------------- |
| Gulp                            | 4.0.0         |
| Gulp CLI                        | 10.15.0       |
| Node.js                         | 2.0.1         |
| NPM                             | 6.4.4         |

## Prerequisites

+ [Node.js](https://nodejs.org) must be installed in order to make use of [NPM](https://www.npmjs.com).
+ [Gulp CLI](https://gulpjs.com) has been used for initiating the **Gulp Tasks**.

## Getting Started

Install the Gulp CLI by running the following command:

```
npm install gulp-cli -g
```

Install the project dependencies by running this command:

```
~\src> npm install
```

Watch your changes in real-time by heading over to **http://localhost:8080** after running this command:

```
~\src> gulp dev
```

Finally, distribute your masterpiece by running this command:

```
~\src> gulp dist
```

> Take a look at **gulpfile.js** to see the tasks in more detail.